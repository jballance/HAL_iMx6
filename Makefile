# Copyright 2017 Castle Technology Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for iMx6 HAL
#

VideoInHAL ?= FALSE

COMPONENT = iMx6_HAL
TARGET = iMx6
OBJS = Top Boot Interrupts Timers CLib CLibAsm UART Debug PRCM Video I2C RTC SDMA TPS Audio GPIO NVMemory KbdScan SATA EtherDrv USB iMx6qboard SDIO SPI Watchdog CPUSpeed PL310 DBell #hdmi_common  hdmi_tx ipu_idmac ipu_common board_hdmi #hdmi_test #CPUClk  hdmi_tx_phy   ips_disp_panel ips_display ipu_image ccm_pll    enet_test  enet_drv  ipu_di hdmi_tx_audio hdmi_print hdmi_audio ipu_dc ipu_dp ipu_dmfc

ifeq (${VideoInHAL},TRUE)
# hdmi_print
OBJS += hdmi_tx_audio  hdmi_audio ipu_dc ipu_dp ipu_dmfc
CFLAGS += -DVideoInHAL
endif
ASFLAGS += -PD "VideoInHAL SETL {${VideoInHAL}}"

include HAL

CFLAGS += -APCS 3/32bit/nofp/noswst
ASFLAGS += -APCS 3/nofp/noswst

# Dynamic dependencies:
